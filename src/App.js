import "./App.css";
import LoginForm from "./components/LoginForm"

function App() {
  return (
    <div className="App">
      <section class="hero">
        <div class="hero-body">
         <LoginForm/>
        </div>
      </section>
      <div class="container is-fullhd">
        <div class="notification">
          Edit the <code>./src</code> folder to add components.
        </div>
      </div>
    </div>
  );
}

export default App;
