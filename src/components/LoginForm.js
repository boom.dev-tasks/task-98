import react from "react";

function LoginForm() {
    return (
        <form className="form">
            <label className="label" for="name">Name</label>
            <input className="input" name="name" type='text'></input>
            <label className="label" for="password">Password</label>
            <input className="input" name="password" type='password'></input>
            <button className="submit" type="submit">Submit</button>
        </form>
    )
}

export default LoginForm